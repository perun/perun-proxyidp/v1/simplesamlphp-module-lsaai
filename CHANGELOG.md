## [4.2.4](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/simplesamlphp-module-lsaai/compare/v4.2.3...v4.2.4) (2024-04-23)


### Bug Fixes

* **deps:** perun v10, refresh lock ([3a73a0d](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/simplesamlphp-module-lsaai/commit/3a73a0db4b07f65a230585826f7fbaab5e980457))

## [4.2.3](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/simplesamlphp-module-lsaai/compare/v4.2.2...v4.2.3) (2024-03-08)


### Bug Fixes

* update dependencies ([17793df](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/simplesamlphp-module-lsaai/commit/17793df6a3e2088f6047fd9aee052e987302bc8f))

## [4.2.2](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/simplesamlphp-module-lsaai/compare/v4.2.1...v4.2.2) (2024-02-27)


### Bug Fixes

* 🐛 spacing when error in login ([2ee4571](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/simplesamlphp-module-lsaai/commit/2ee45710b989fa9e4a9373e8e4b53b426841ead0))

## [4.2.1](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/simplesamlphp-module-lsaai/compare/v4.2.0...v4.2.1) (2023-12-29)


### Bug Fixes

* 🐛 Fix missing separator and pwd reset link ([412c095](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/simplesamlphp-module-lsaai/commit/412c09534f5928e3cf09e166563293692336876a))

# [4.2.0](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/simplesamlphp-module-lsaai/compare/v4.1.0...v4.2.0) (2023-09-13)


### Features

* 🎸 Forgotten username flow ([36ef4ec](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/simplesamlphp-module-lsaai/commit/36ef4ec06b5108961f5ba0db8483c0a68c8aaa30))

# [4.1.0](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/simplesamlphp-module-lsaai/compare/v4.0.0...v4.1.0) (2023-07-19)


### Features

* 🎸 Forward URLs to get back to login after registration ([512dfe3](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/simplesamlphp-module-lsaai/commit/512dfe376cb20168e5557216e5157e264aaca6fe))

# [4.0.0](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/simplesamlphp-module-lsaai/compare/v3.1.1...v4.0.0) (2023-07-19)


### Code Refactoring

* 💡 Remove old LS Hostel ([13b8dbf](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/simplesamlphp-module-lsaai/commit/13b8dbf11457932c93fc8dfbc313d85df0123b4b))
* 💡 Removed obsolete code ([f9b8ac3](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/simplesamlphp-module-lsaai/commit/f9b8ac3725352fe56e2cafcd1eda2cdfe7b07f98))
* 💡 Rename LS Hostel to LS Username Login ([2ef33bd](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/simplesamlphp-module-lsaai/commit/2ef33bd87eaf41be57548e9efa52af8bd384d3d1))


### BREAKING CHANGES

* 🧨 Removed LoginModify AuthProc filter
* 🧨 modified config options - see example config. Also renamed directory
under "themes"
* 🧨 Removed LS Hostel old

## [3.1.1](https://github.com/CESNET/lsaai-local-idp-ssp-template/compare/v3.1.0...v3.1.1) (2022-01-25)


### Bug Fixes

* change required version of perun module ([265bc98](https://github.com/CESNET/lsaai-local-idp-ssp-template/commit/265bc984b961c97a20c6766911718af26e7e838c))

# [3.1.0](https://github.com/CESNET/lsaai-local-idp-ssp-template/compare/v3.0.0...v3.1.0) (2022-01-17)


### Features

* 🎸 New logo ([6db7c0f](https://github.com/CESNET/lsaai-local-idp-ssp-template/commit/6db7c0fa182f21b24a98f849d5b2d27ecf3e9a91))

# [v2.0.0](https://github.com/CESNET/lsaai-local-idp-ssp-template/compare/v1.3.0...v2.0.0)

## [v1.3.0]
#### Changed
- Redesign LS Hostel theme

## [v1.2.0]
#### Added
- Added new filter which converts login domain to lowercase and trim whitespaces
- Added .gitignore

#### Changed
- Some fields on forms are now prefilled if the data are accessible 

## [v1.1.0]
#### Added
- Added possibility to recover LS Hostel password
#### Changed
- Use email as identifier instead of username


## [v1.0.0]
- First release
 
[v1.3.0]: https://github.com/CESNET/lsaai-local-idp-ssp-template/tree/v1.3.0
[v1.2.0]: https://github.com/CESNET/lsaai-local-idp-ssp-template/tree/v1.2.0
[v1.1.0]: https://github.com/CESNET/lsaai-local-idp-ssp-template/tree/v1.1.0
[v1.0.0]: https://github.com/CESNET/lsaai-local-idp-ssp-template/tree/v1.0.0
